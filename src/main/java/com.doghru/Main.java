package com.doghru;

public class Main {
    public static void main(String args[]) {
        int count =0;
        Console console = new Console();
        console.getFirstN();
        console.getSecondN();
        console.print();
        Fibonacci fibonacci = new Fibonacci();
        OddEven oddEven = new OddEven();
        int array[] = fibonacci.fibArray(console.getSecondN() - 1, console.getSecondN(), 5);
        System.out.print("Fibonacci array : ") ;
        for (int i = 0; i < array.length; i++) {
            if(oddEven.isOdd(array[i])){
                 count ++;
            }
            System.out.print(" " + array[i]);
        }
        System.out.println("\n"+"Percentage of odd : "+ (double)count/array.length*100);
        System.out.println("\n"+"Percentage of even : "+ (100-(double)count/array.length*100));
    }
}