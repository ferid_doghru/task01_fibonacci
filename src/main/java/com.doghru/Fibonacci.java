package com.doghru;

public class Fibonacci {

    public  int[] fibArray(int a,int b ,int size) {
        int[] arr = new int[size];
        arr[0] = a;
        arr[1] = b;
        for (int i = 2; i < size; i++) {
            arr[i] = arr[i - 2] + arr[i - 1];
        }
        return arr;
    }
}



