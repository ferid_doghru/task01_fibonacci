package com.doghru;

import java.util.Scanner;

public class Console {
  private    int firstN;
  private   int secondN;
  private   int odd[];
  private   int even[];
  private   int sumO;
  private   int sumE;
    private Scanner scanner;

    public int getFirstN() {
        scanner = new Scanner(System.in);
        try {
            System.out.println("Enter first number ");
            firstN = scanner.nextInt();
        } catch (RuntimeException e) {
            System.out.println("Try again");
            return getFirstN();
        }
        return firstN;
    }

    public int getSecondN() {
        scanner = new Scanner(System.in);
        try {
            System.out.println("Enter second number ");
            secondN = scanner.nextInt();
        } catch (RuntimeException e) {
            System.out.println("Try again");
            return getSecondN();
        }
        return secondN;
    }

    public void print() {
        OddEven odd = new OddEven();
        OddEven even = new OddEven();
        System.out.println("Our odd numbers : ");
        for (int i = firstN; i <= secondN; i++) {
            if (odd.isOdd(i)) {
                System.out.print(i+" ");
                sumO += i;
            }
        }
        System.out.println("\n"+"Sum of odd numbers is : " +sumO);
        System.out.println();
        System.out.println("Our even numbers : ");
        for (int i = secondN; i >= firstN; i--) {
            if (even.isEven(i)) {
                System.out.print(i+" ");
                sumE +=i;
            }
        }
        System.out.println("\n"+"Sum of even numbers is : " +sumE);
    }
}